#!/bin/bash

# The input .csr is in ./csrs directory. It's in the <name>.csr format.
input=$1
caName=$2
encoding=$3
if [[ -z ${input} ]]; then
    echo "Usage: $(basename $0) req.csr"
    exit 1
fi

filename=$(basename -- ${input})
name=${filename%.*}
ext=${filename##*.}

if [[ ${ext} != "csr" ]]; then
    echo "$(basename $0) failed. Input file CSR file not in .csr extension."
    exit 1
fi

basedir=${PWD}
up1="$(dirname "${basedir}")"
up2="$(dirname "${up1}")"
up3="$(dirname "${up2}")"
frontend_dir=${up3}/frontend/public/${caName}
echo ${frontend_dir}
cert=${name}.crt
openssl ca -batch -config ca.conf -in ${input} -out certs/${name}.crt

if [ $3 = "PEM" ]; then
    cp certs/${name}.crt ${frontend_dir}/
else
    openssl crl2pkcs7 -nocrl -certfile certs/${name}.crt -out certs/${name}.p7b
    cp certs/${name}.p7b ${frontend_dir}/
fi

rm certs/*.pem
rm csrs/*.csr
rm certs/*.crt
rm certs/*.p7b

exit 0