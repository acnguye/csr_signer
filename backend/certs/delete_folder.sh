folder=$1
if (($# < 1)); then
    echo "Usage: $(basename $0) Folder_Name"
    echo "Description: Script to delete all folders/files for a CA."
    exit 1
fi

basedir=${PWD}
up1="$(dirname "${basedir}")"
up2="$(dirname "${up1}")"
backend_dir=${basedir}/${folder}
frontend_dir=${up2}/frontend/public
CAs=${up2}/frontend/public/CAs

#remove folder in backend/certs
rm -rf ${folder}

#rm folder in frontend/public
cd ${frontend_dir}
rm -rf ${folder}

#rm folder.crt in frontend/public/CAs
cd ${CAs}
rm ${folder}.crt