#
# OpenSSL configuration for the Root Certification Authority.
#

#
# Default Certification Authority
#
[ ca ]
default_ca              = CA_default

#
# Root Certification Authority
#
[ CA_default ]
private_key             = ca_data/ca.key
certificate             = ca_data/ca.crt
database                = ca_data/index.txt
serial                  = ca_data/serial
crlnumber               = ca_data/crlnum
crl                     = ca_data/crl.crt
certs                   = ./certs
new_certs_dir           = ./certs
crl_dir                 = ./crl
default_days            = 365
default_crl_days        = 60
default_md              = sha256
name_opt                = ca_default
cert_opt                = ca_default
preserve                = no
email_in_dn             = no
unique_subject          = no
policy                  = policy
x509_extensions         = user_cert
copy_extensions         = copy
crl_extensions          = crl_ext

#
# User Certificate Extensions
#
[ user_cert ]
basicConstraints        = CA:false
keyUsage                = digitalSignature, keyAgreement, keyEncipherment
extendedKeyUsage        = serverAuth
subjectKeyIdentifier    = hash
authorityKeyIdentifier  = keyid:always, issuer
issuerAltName           = issuer:copy

#
# Distinguished Name Policy for CAs
#
[ policy ]
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = supplied
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

#
# Root CA Request Options
#
[ req ]
default_bits            = 2048
default_md              = sha256
string_mask             = utf8only
utf8                    = yes
prompt                  = no
distinguished_name      = distinguished_name
x509_extensions         = root_ca

#
# Distinguished Name (DN)
#
[ distinguished_name ]
countryName             = US
stateOrProvinceName     = NY
localityName            = New York City
organizationName        = Star Professional CA, LLC
organizationalUnitName  = Consumer CA Dept
commonName              = Star Professional Root Certification Authority
emailAddress            = questions@starpro.com

#
# Root CA Certificate Extensions
#
[ root_ca ]
basicConstraints        = critical, CA:true
keyUsage                = critical, keyCertSign, cRLSign
subjectKeyIdentifier    = hash
subjectAltName          = @subject_alt_name
authorityKeyIdentifier  = keyid:always
issuerAltName           = issuer:copy

#
# Certificate Authorities Alternative Names
#
[ subject_alt_name ]
URI                     = http://www.starpro.com/
DNS.0                   = www.starpro.com
email                   = questions@starpro.com

#
# CRL Certificate Extensions
#
[ crl_ext ]
authorityKeyIdentifier  = keyid:always
issuerAltName           = issuer:copy

# EOF
