folder=$1
if (($# < 1)); then
    echo "Usage: $(basename $0) Folder_Name"
    echo "Description: Script to create all the files/folders necessary for a CA"
    exit 1
fi
basedir=${PWD}
up1="$(dirname "${basedir}")"
up2="$(dirname "${up1}")"
backend_dir=${basedir}/${folder}
frontend_dir=${up2}/frontend/public

cities=(Aberdeen Abilene Akron Albany Albuquerque Alexandria Allentown Amarillo Anaheim Anchorage Ann Arbor Antioch Apple Valley Appleton Arlington Arvada Asheville Athens Atlanta Atlantic City Augusta Aurora Austin Bakersfield Baltimore Barnstable Baton Rouge Beaumont Bel Air Bellevue Berkeley Bethlehem Billings Birmingham Bloomington Boise Boise City Bonita Springs Boston Boulder Bradenton Bremerton Bridgeport Brighton Brownsville Bryan Buffalo Burbank Burlington Cambridge Canton Cape Coral Carrollton Cary Cathedral City Cedar Rapids Champaign Chandler Charleston Charlotte Chattanooga Chesapeake Chicago Chula Vista Cincinnati Clarke County Clarksville Clearwater Cleveland College Station Colorado Springs Columbia Columbus Concord Coral Springs Corona Corpus Christi Costa Mesa Dallas Daly City Danbury Davenport Davidson County Dayton Daytona Beach Deltona Denton Denver Des Moines Detroit Downey Duluth Durham El Monte El Paso Elizabeth Elk Grove Elkhart Erie Escondido Eugene Evansville Fairfield Fargo Fayetteville Fitchburg Flint Fontana Fort Collins Fort Lauderdale Fort Smith Fort Walton Beach Fort Wayne Fort Worth Frederick Fremont Fresno Fullerton Gainesville Garden Grove Garland Gastonia Gilbert Glendale Grand Prairie Grand Rapids Grayslake Green Bay GreenBay Greensboro Greenville Gulfport-Biloxi Hagerstown Hampton Harlingen Harrisburg Hartford Havre de Grace Hayward Hemet Henderson Hesperia Hialeah Hickory High Point Hollywood Honolulu Houma Houston Howell Huntington Huntington Beach Huntsville Independence Indianapolis Inglewood Irvine Irving Jackson Jacksonville Jefferson Jersey City Johnson City Joliet Kailua Kalamazoo Kaneohe Kansas City Kennewick Kenosha Killeen Kissimmee Knoxville Lacey Lafayette Lake Charles Lakeland Lakewood Lancaster Lansing Laredo Las Cruces Las Vegas Layton Leominster Lewisville Lexington Lincoln Little Rock Long Beach Lorain Los Angeles Louisville Lowell Lubbock Macon Madison Manchester Marina Marysville McAllen McHenry Medford Melbourne Memphis Merced Mesa Mesquite Miami Milwaukee Minneapolis Miramar Mission Viejo Mobile Modesto Monroe Monterey Montgomery Moreno Valley Murfreesboro Murrieta Muskegon Myrtle Beach Naperville Naples Nashua Nashville New Bedford New Haven New London New Orleans New York New York City Newark Newburgh Newport News Norfolk Normal Norman North Charleston North Las Vegas North Port Norwalk Norwich Oakland Ocala Oceanside Odessa Ogden Oklahoma City Olathe Olympia Omaha Ontario Orange Orem Orlando Overland Park Oxnard Palm Bay Palm Springs Palmdale Panama City Pasadena Paterson Pembroke Pines Pensacola Peoria Philadelphia Phoenix Pittsburgh Plano Pomona Pompano Beach Port Arthur Port Orange Port Saint Lucie Port St. Lucie Portland Portsmouth Poughkeepsie Providence Provo Pueblo Punta Gorda Racine Raleigh Rancho Cucamonga Reading Redding Reno Richland Richmond Richmond County Riverside Roanoke Rochester Rockford Roseville Round Lake Beach Sacramento Saginaw Saint Louis Saint Paul Saint Petersburg Salem Salinas Salt Lake City San Antonio San Bernardino San Buenaventura San Diego San Francisco San Jose Santa Ana Santa Barbara Santa Clara Santa Clarita Santa Cruz Santa Maria Santa Rosa Sarasota Savannah Scottsdale Scranton Seaside Seattle Sebastian Shreveport Simi Valley Sioux City Sioux Falls South Bend South Lyon Spartanburg Spokane Springdale Springfield St. Louis St. Paul St. Petersburg Stamford Sterling Heights Stockton Sunnyvale Syracuse Tacoma Tallahassee Tampa Temecula Tempe Thornton Thousand Oaks Toledo Topeka Torrance Trenton Tucson Tulsa Tuscaloosa Tyler Utica Vallejo Vancouver Vero Beach Victorville Virginia Beach Visalia Waco Warren Washington Waterbury Waterloo West Covina West Valley City Westminster Wichita Wilmington Winston Winter Haven Worcester Yakima Yonkers York Youngstown)
states=(AL AK AS AZ AR CA CO CT DE DC FM FL GA GU HI ID IL IN IA KS KY LA ME MH MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND MP OH OK OR PW PA PR RI SC SD TN TX UT VT VI VA WA WV WI WY)

state_size=${#states[@]}
state_index=$(($RANDOM % $state_size))
state=${states[$state_index]}

city_size=${#cities[@]}
city_index=$(($RANDOM % $city_size))
city=${cities[$city_index]}


mkdir ${folder}
cp ca_template/* ${folder}
cd ${folder}

sed -i -e "s/NY/${state}/g" ca.conf
sed -i -e "s/New York City/${city}/g" ca.conf
sed -i -e "s/Star Professional/${folder} Incorporated/g" ca.conf
sed -i -e "s/starpro/${folder}/g" ca.conf
./ca_init.sh

cd certs
touch random.txt
cd ..
cd crl
touch random.txt
cd ..
cd csrs
touch random.txt

cd ${frontend_dir}
mkdir ${folder}
cd CAs
cp ${backend_dir}/ca_data/ca.crt .
mv ca.crt ${folder}.crt
