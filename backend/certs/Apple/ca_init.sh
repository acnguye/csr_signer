#!/bin/bash

# create directories and files
[[ ! -d ca_data ]] && /bin/mkdir ca_data
[[ ! -d certs ]] && /bin/mkdir certs
[[ ! -d csrs ]] && /bin/mkdir csrs
[[ ! -d crl ]] && /bin/mkdir crl
[[ ! -f ca_data/serial ]] && echo "64" >> ca_data/serial
[[ ! -f ca_data/index.txt ]] && touch ca_data/index.txt
[[ ! -f ca_data/crlnum ]] && echo "0" >> ca_data/crlnum

# create ca key and cert
openssl genrsa -out ca_data/ca.key 4096
openssl req -new -sha256 -x509 -key ca_data/ca.key -days 365 -config ca.conf -out ca_data/ca.crt

exit 0
