const express = require("express");
const router = express.Router();
const shell = require("shelljs");

router.post("/", function (req, res, next) {
  if (!req.files) {
    res.send("File was not found");
    return;
  }
  var date = new Date(Date.now());
  const file_name = date.toISOString() + ".csr";

  const caName = req.body.caName;
  const encoding = req.body.encoding;
  const cert = req.files.cert;
  shell.cd(__dirname + "/../certs/" + caName + "/");

  cert.mv("./csrs/" + file_name, function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      shell.exec("exec ./sign_csr.sh csrs/" + file_name + " " + caName + " " + encoding);
      res.json(file_name);
    }
  });
});

module.exports = router;
