const express = require('express');
const router = express.Router();
const shell = require('shelljs');
const path = require('path');
const fs = require('fs');

router.get('/', function(req, res, next) {
    const directoryPath = path.join(__dirname + "/../certs");
    console.log(directoryPath);
    var files = fs.readdirSync(directoryPath)
    var path_list = []
    var first = true
    for (var file of files) {
        if(file == "ca_template" || file == "create_folder.sh" || file == "readme.txt" || file == "delete_folder.sh"){
            continue;
        }
        folder = {}
        folder['name'] = file
        if (first) {
            folder['first'] = true
            first = false
        } else {
            folder['first'] = false
       }
       path_list.push(folder)
    }
    res.json(path_list);
});

module.exports = router;
