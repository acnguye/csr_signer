const express = require("express");
const router = express.Router();
const path = require("path");
const fs = require("fs");

router.post("/", function (req, res, next) {
  const directoryPath = path.join(
    __dirname + "/../../frontend/public/" + req.body.name + "/"
  );
  console.log(directoryPath);
  var files = fs.readdirSync(directoryPath);
  files.reverse();
  var path_list = [];
  for (var file of files) {
    let v = {};
    let loc = "/" + req.body.name + "/" + file;
    v["location"] = loc;
    v["name"] = file;
    path_list.push(v);
  }
  res.json(path_list);
});

module.exports = router;
