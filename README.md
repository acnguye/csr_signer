# How to run server on Ubuntu 16.04
1. Clone the repo
2. Run 'cd csr_signer'
3. Run 'sudo apt update'
4. Run 'sudo apt install nodejs npm'
5. Make sure nodejs is installed with 'nodejs --version'
6. Run 'npm install'
7. Rn 'npm run server-with-client'
8. Open localhost:3000 on your browser