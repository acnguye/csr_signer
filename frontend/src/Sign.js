import React, { useEffect, useRef, useState } from "react";
import "./Sign.css";

function Sign(props) {
  const fileInputRef = useRef();
  const [selectedFile, setSelectedFile] = useState();
  const [encoding, setEncoding] = useState("PEM");
  const [showBeingCreated, setBeingCreated] = useState(false);
  const [showSigningError, setSigningError] = useState(false);
  const [showSignedCSRName, setShowSignedCSRName] = useState(false);
  const [disableButton, setDisableButton] = useState("disabled");
  const [signedCSRName, setSignedCSRName] = useState("");

  const [currCA, setCA] = useState("");
  const [signedCAs, setSignedCAs] = useState([]);

  useEffect(() => {
    setCA(props.caName);
    setSignedCAs(props.signedCAs);
  },[props.caName, props.signedCAs]);

  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0]);
    setDisableButton("");
  };
  const handleSubmission = (event) => {
    event.preventDefault();
    const formData = new FormData();
    formData.append("cert", selectedFile);
    formData.append("caName", currCA);
    formData.append("encoding", encoding);
    setBeingCreated(true);
    console.log(formData);
    fetch("/sign", {
      method: "POST",
      body: formData,
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setSignedCSRName(data);
        setBeingCreated(false);
        props.update();
        fileInputRef.current.value = null;
        setSigningError(false);
        setSelectedFile("");
        setShowSignedCSRName(true);
      })
      .catch((error) => {
        console.log("got in error code");
        console.error("error ", error);
        setBeingCreated(false);
        setSigningError(true);
      });
  };
  return (
    <div class="right">
      <div class="container">
        {props.switchBtn ? (
          <div>
            <div class="title">
              <h1>Download {currCA}'s Root CA Certificate</h1>
            </div>
            <div class="spacing bottom-spacing">
              <a href={"/CAs/" + currCA + ".crt"} download>
                <button type="button" class="btn btn-success">
                  Click to Download
                </button>
              </a>
            </div>
          </div>
        ) : (
          <div>
            <div class="title">
              <h1>Sign CSR with {currCA} Root CA</h1>
            </div>
            <div class="spacing">
              <form onSubmit={handleSubmission}>
                <div class="bottom-spacing">
                  <p>Please upload a .csr file for {currCA} CA to sign.</p>
                  <input
                    type="file"
                    name="cert"
                    onChange={changeHandler}
                    accept=".csr"
                    ref={fileInputRef}
                    required
                  />
                </div>
                <div>
                  <label>Choose Encoding:</label>
                </div>
                <div class="form-check form-check-inline bottom-spacing">
                  <input
                    class="form-check-input"
                    type="radio"
                    name="inlineRadioOptions"
                    id="inlineRadio1"
                    value="PEM"
                    onChange={(e) => setEncoding(e.target.value)}
                    checked
                  />
                  <label class="form-check-label" for="inlineRadio1">
                    PEM
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <input
                    class="form-check-input"
                    type="radio"
                    name="inlineRadioOptions"
                    id="inlineRadio2"
                    value="PKCS7"
                    onChange={(e) => setEncoding(e.target.value)}
                  />
                  <label class="form-check-label" for="inlineRadio2">
                    PKCS7
                  </label>
                </div>
                <br></br>
                <button class="btn btn-success" disabled={disableButton}>
                  Sign Request
                </button>
              </form>
            </div>
            {showBeingCreated ? (
              <div class="spacing">
                <p>
                  Your certificate is signing, please wait. A link to download
                  will be shown soon in the list below.{" "}
                  <img src="/loading.gif" alt="loading" id="loading"></img>
                </p>
              </div>
            ) : null}
            {showSigningError ? (
              <div class="spacing">
                <p id="error">
                  There was an error signing the CSR. Please try to refresh the
                  page and re-upload the file. If the problem still presist.
                  Please contact Andy Nguyen at andy.nguyen@microchip.com to fix
                  it.
                </p>
              </div>
            ) : null}
            <div class="title">
              <h1>List of Signed Requests from {currCA} Root CA</h1>
            </div>
            <div class="spacing bottom-spacing">
              <p>
                Click the download button to download the signed CSR by {currCA}{" "}
                Root CA
              </p>
              {showSignedCSRName ? (
                <p>Your signed CSR file name is {signedCSRName}</p>
              ) : null}
              <div class="tableFixHead">
                <table>
                  <tr>
                    <th>List of Signed CSRs</th>
                    <th>Download</th>
                  </tr>
                  {signedCAs.map((signedCA, index) => (
                    <tr>
                      <td>
                        {index + 1}. {signedCA.name}
                      </td>
                      <td>
                        <a href={signedCA.location} download>
                          <button type="button" class="btn btn-success">
                            Download
                          </button>
                        </a>
                      </td>
                    </tr>
                  ))}
                </table>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
export default Sign;
