import React, { useEffect, useState } from "react";
import "./Sidebar.css";

function Sidebar(props) {
  const [listCA, setListCa] = useState([]);

  useEffect(() => {
    fetch("/listca")
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setListCa(data);
        props.change(data[0].name);
      });
  }, []);

  const changeHandler = (event) => {
    props.change(event.target.value);
  };
  return (
    <div class="container">
      <div class="inside-spacing">
        <h2 class="h1_sidebar">List of Root CAs (Certificate Authorities)</h2>
        <select
          class="form-select form-select-lg"
          multiple
          aria-label="multiple select example"
          onChange={changeHandler}
        >
          {listCA.map((ca) =>
            ca.first ? (
              <option value={ca.name} selected>
                {ca.name}
              </option>
            ) : (
              <option value={ca.name}>{ca.name}</option>
            )
          )}
        </select>
      </div>
    </div>
  );
}
export default Sidebar;
