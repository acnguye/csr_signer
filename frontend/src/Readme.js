import { useState } from "react";
import "./Readme.css";

function Readme({ caName }) {
  const [showSignCSR, setShowSignCsr] = useState(true);
  const [btnText, setBtnText] = useState("Install CSR and Root CA Instructions");

  const changeHandler = (event) => {
    setShowSignCsr(!showSignCSR);
    if (btnText === "Install CSR and Root CA Instructions") {
      setBtnText("Sign CSR Instructions");
    } else {
      setBtnText("Install CSR and Root CA Instructions");
    }
  };

  return (
    <div class="instruction-container">
      <div class="instructions">
        {showSignCSR ? (
          <div>
            <h2>
              <u>How to Sign CSR</u>
            </h2>
            <ol>
              <li>
                Go to your K2 and go to{" "}
                <strong>Security/X509 SS Cert CS Req.</strong>
              </li>
              <li>
                Select Certificate Signing Request (.csr) and configure the CSR.
              </li>
              <ul>
                <li>
                  <strong>
                    Make sure to input your K2's IP address into slot IP1 in the
                    configuration. If you do not do this, the certificate will
                    not be valid.
                  </strong>
                </li>
              </ul>
              <li>
                Add the CSR and download it. Then come back to this website to
                sign it.
              </li>
              <li>Upload the .csr file.</li>
              <ul>
                <li>File should be #_req.csr.</li>
              </ul>
              <li>
                Click <strong>Sign Request.</strong>
              </li>
              <li>Wait for csr to be signed by the certficate you chose.</li>
              <li>
                Download the first .crt you see in the list of signed requests.
                <ul>
                  <li>
                    The list of signed requests are ordered from newest to
                    oldest.
                  </li>
                  <li>
                    You will also see the name of the file just in case someone
                    else signs a CSR with the same Root CA
                  </li>
                </ul>
              </li>
              <li>
                Click the button <strong>Switch to Download CAs</strong>.
              </li>
              <li>Download {caName} Root CA.</li>
              <li>
                Click button below to learn how to install the signed CSR on
                your K2 and how to install {caName} Root CA into Chrome.
              </li>
            </ol>
          </div>
        ) : (
          <div>
            <h2>
              <u>How to Install CRT</u>
            </h2>
            <ol>
              <li>
                <strong>
                  Skip to step 9 if you already installed the {caName} Root CA
                  into Chrome. You only need to do this once.
                </strong>
              </li>
              <li>Open the settings in Chrome, 3 dots in top right corner.</li>
              <li>
                Scroll down to <strong>Security</strong> and click it.
              </li>
              <li>
                Scroll down to <strong>Manage Certificate</strong> and click it.
              </li>
              <li>
                Cick <strong>Trusted Root Certification Authorities</strong>,
                near the top. It should be 3rd option.
              </li>
              <li>Click import and next. Upload {caName}.crt.</li>
              <li>
                Click next and click next again. Then click finish. Click 'yes'
                to install the certificate.
              </li>
              <li>
                Scroll down the list of root certificates to make sure {caName}{" "}
                Root CA was installed.
              </li>
              <ul>
                <li>The order is alphabetical.</li>
                <li>The name should be company_name Incorporation.</li>
                <li>eg. {caName} Incorporation</li>
              </ul>
              <li>
                Go to <strong>X.509 install</strong> on K2.
              </li>
              <li>
                Find the CSR you created and install the signed CRT to that
                specific CSR. Should be the file that has the date.
              </li>
              <ul>
                <li> (eg. 2021-11-04T22:47:30.493Z.crt)</li>
                <li>
                  Upload it to Certificate section and with PEM encoding
                  selected.
                </li>
              </ul>
              <li>
                Go to <strong>X.509 Mapping</strong> and select your CRT for
                HTTPS.
              </li>
            </ol>
            <div id="steps">
              <strong>
                Follow the steps below if you do not see 'Not secure' in the
                URL.
              </strong>
            </div>
            <ol>
              <li>Click not secure icon next to url.</li>
              <ul>
                <li>
                  Make sure the certificate is <strong>valid</strong>.
                </li>
                <li>
                  If the certificate is <strong>not valid</strong>, most likely
                  means you did not add the K2's IP address to your CSR.
                </li>
                <li>
                  Please add your K2's IP address to IP1 in the config and
                  redownload and resign the CSR and reinstall it in <strong>X.509 Certificates Install</strong>.
                </li>
              </ul>
              <li>Click <strong>site settings</strong>.</li>
              <li>Click <strong>Clear data</strong></li>
              <li>Click <strong>Privacy and security</strong> on the left hand side</li>
              <li>Click <strong>Clear browsing data</strong></li>
              <li>Set <strong>Time range</strong> to <strong>All time</strong></li>
              <li>Click <strong>Clear Data</strong></li>
              <ul>
                <li>You don't have to clear browser history.</li>
                <li>Need to clear <strong>Cookies and other site data</strong> and <strong>Cached images and files</strong> </li>
              </ul>
              <li>Restart your browser.</li>
              <li>
                You should now have a lock symbol next to the URL and if you
                click the lock icon it should say <strong>Connection is secure</strong>
              </li>
            </ol>
          </div>
        )}
        <div class="instruction-btn">
          <button
            id="howto-btn"
            type="button"
            class="btn btn-secondary"
            onClick={changeHandler}
          >
            {btnText}
          </button>
        </div>
      </div>
    </div>
  );
}
export default Readme;
