//import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import "./App.css";
import Sign from "./Sign";
import Sidebar from "./Sidebar";
import Readme from "./Readme";
import { useState, useEffect } from "react";
function App() {
  const [signedCAs, setSignedCAs] = useState([]);
  const [caName, setCAName] = useState("");
  const [downloadOrSign, setDownloadOrSign] = useState(false);
  const [btnText, setBtnText] = useState('Switch to Download CAs');

  useEffect(() => {
    updateCAList();
  }, [caName]);

  const switchToSign = () =>{
    setDownloadOrSign(!downloadOrSign);
    if(downloadOrSign){
      setBtnText('Switch to Download CAs');
    } else {
      setBtnText('Switch to Signing CSRs');
    }
  }

  const updateCAList = () => {
    fetch("/signedcas", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ name: caName }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setSignedCAs(data);
      });
  };
  return (
    <main>
      <img src="/logo.png" id="logo"></img>
      <div className="grid-container">
        <div className="sidebar">
          <Readme caName={caName}/>
        </div>
        <div className="stuff">
          <Sidebar change={(value) => setCAName(value)} update={updateCAList} />
          <button type="button" class="btn btn-secondary" onClick={switchToSign}>{btnText}</button>
          <Sign caName={caName} signedCAs={signedCAs} update={updateCAList} switchBtn={downloadOrSign} />
        </div>
      </div>
    </main>
  );
}

export default App;
